\section{\acs{siuv}}\label{sec:siuv}
In this section, we describe the \ac{sts} in addition to the architecture of \acs{siuv}.

\subsection{\acf{ucsp}}\label{sec:ucsp}
Lazouski et al. \cite{ucs2012prototype} introduced a \ac{ucs} prototype that realises the \ac{ucon} model.
They used a policy language that extends \ac{xacml} semantics with \ac{ucon} novelties and defines an architecture to enforce \ac{ucon} policies.
The language adds an implicit temporal state that is captured by classifying policy rules as ``\emph{pre}'', ``\emph{ongoing}'' and ``\emph{post}''.
\ac{ucs} enforces continuous usage control by adding an authorisation session, continuous monitoring of attributes and re-evaluation of relevant policies when a change occurs.
However, sessions in \ac{ucs} are limited to the duration of active authorisations only.
This is not sufficient for applications, such as smart vehicles, that include continuous interactions before or after access (e.g.ensure safe release of resources).
This was among the motivations for our team to introduce \ac{ucsp}: an enhanced and optimised \ac{ucon} framework that conserves a full \ac{abac} baseline and supports auxiliary evaluators like trust/confidence level.
\ac{ucsp} extends authorisation sessions to cover continuous interactions and monitoring before granting access, during authorisation and during revocation of access in order to support pre- and post-usage interactions such as multi-factor authentication, safe revocation, etc. (e.g. safely stop the vehicle before completely revoking access).
\ac{ucsp} uses \ac{alfa} - instead of \ac{xacml} - as the baseline policy language, which results in a considerable improvement in performance and efficiency due to \ac{alfa}'s compactness.
\ac{ucsp} leverages the publish/subscribe pattern to maximize concurrency between policy parsing and evaluation, and attribute retrieval.
This maximizes performance and minimizes the need for high network speeds or high computational resources.
\ac{ucsp} also improves the ability to upgrade or substitute component services and migrate to a distributed deployment where necessary.
We presented, in \cite{theo2020uconplus}, a variant of \ac{ucsp} that integrates a trust level evaluation engine and is tailored for zero-trust \ac{iot} networks.
In this paper, we describe another variant that is optimised for efficient continuous authorisation in embedded systems including automotive units.
Figure \ref{fig:ucs_architecture} illustrates the architecture of \ac{ucsp}, which consists of 8 major components as follows:
\texttt{\ac{ch}} is the core component that receives access requests and manages authorisation workflows.
\texttt{Message Bus} supports communications between components using the \ac{pubsub} pattern.
\texttt{\ac{pep}} is the interface of \ac{ucsp} and the component that protects resources. It creates access requests, invokes the \ac{ch} and enforces decisions.
\texttt{\ac{pdp}} is the component that evaluates policies and makes access decisions.
\texttt{\ac{pap}} stores and manages policies and is used by the \ac{pdp} to retrieve applicable policies.
\texttt{\ac{pip}} defines where to find attributes and how to monitor them.
\texttt{PIP Registry} manages \acp{pip} and defines which \acp{pip} are responsible for which attributes.
\texttt{\ac{sm}} manages and keeps track of all ongoing sessions to support the continuity of control. 
\texttt{\ac{at}} is a cache of attribute values and other metadata.
\texttt{\ac{ar}} is an auxiliary component in charge of
querying and updating attribute values.
\texttt{\ac{om}} handles and manages policy and rule obligations.

\begin{figure}[!htb]
	\centering
	\includegraphics[scale=0.5]{figures/ucs_architecture.png}
	\caption{\ac{ucs}+ architecture}
	\label{fig:ucs_architecture}
\end{figure}

\subsection{\acs{siuv} \acf{sts}}\label{sec:sts}
In a typical \ac{sts}, a particular set of privileges may be bound to specific attributes.
Thus, all subjects that have these attributes will always have the same privileges regardless of the context.
In addition, issued privileges do not change even if attributes change.
This usually results in subjects being either overprivileged or underprivileged.
To solve such issues, we introduce a stateful policy-based \ac{sts} that exchanges external \acp{vc} about identity claims (identity \acp{vc}) with internal contextualised \acp{vc} that determine the privileges of the corresponding subject (privilege \acp{vc}).
The \ac{sts} model shares some common principles with \ac{ucsp}, namely policy-based decision making, session-based continuous monitoring, as well as policy re-evaluation and revision.
While \ac{ucsp} uses these concepts to manage authorisations, the \ac{sts} uses them to improve the dynamicity as well as situation and change awareness throughout the life-cycle of identity claims and privileges.
The \ac{sts} employs policy-based decision making in order to dynamically exchange identity claims with privileges according to situation-aware policies.
In addition, session-based continuous monitoring allows the \ac{sts} to manage a continuous lifecylce of privileges starting from their issuance and lasting until their revocation or expiry.
The privilege life-cycle may also last beyond revocation in order to support post-revocation interactions such as graceful revocation or safety actions.
Finally, policy re-evaluation allows the \ac{sts} to react to changes in identity claims or environmental conditions, which may result in privilege escalation, degradation or revocation.
The architecture of the \ac{sts} is shown in Figure \ref{fig:sts_architecture}.
The \ac{pep} receives identity \acp{vc} from the subject, verifies them using the Claims Verifier subcomponent, then sends a privileges request to the \ac{ch}.
The \ac{ch} retrieves relevant policies from the \ac{pap}, collects required attributes from \acp{pip} then invokes the \ac{sm} to create a session and manage the life-cycle of the privileges to be issued.
Thereupon, the \ac{ch} invokes the \ac{pdp} to evaluate the policies, then returns the evaluation decision to the Privileges Issuer subcomponent of \ac{pep}, which issues privilege \acp{vc}.

We use \ac{alfa} policies to determine how the \ac{sts} issues privileges according to identity claims and environmental attributes.
We particularly use rule obligations to determine the specific privileges that must be issued.
Thus, each rule is used to define a set of privileges to be issued if the conditions of the rule are met. 
This allows a fine-grained control on how to issue privileges according to attribute values.
We use the ``permitUnlessDeny'' combining algorithm, which combines all obligations from all applicable rules that evaluate into permit as long as no deny rule apply.
An example policy is shown in Listing \ref{lst:driver_sts_policy}.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=\linewidth]{figures/sts_architecture.png}
	\caption{\acs{siuv} \ac{sts} architecture}
	\label{fig:sts_architecture}
\end{figure}

\subsection{\acs{siuv} Architecture}\label{sec:siuv_architecture}
In this section, we present the architecture of \acs{siuv} as shown in Figure \ref{fig:iam_architecutre}.
The \ac{sts} runs on the general-purpose computer gateway, whereas \acp{ucsp} run on the \acp{ecu} that they protect.
The \ac{vdr} is a logical component and is implemented as a distributed hashtable.
Since \ac{siuv} is based on \ac{soa}, all communications use the Ethernet protocol.
Moreover, AUTOSAR specifies the SOME/IP\footnote{\url{https://www.autosar.org/fileadmin/user_upload/standards/foundation/1-0/AUTOSAR_PRS_SOMEIPProtocol.pdf}} protocol that supports TLS, thus communications between \ac{siuv} components are protected. 
Furthermore, additional features, such as end-to-end encryption\footnote{\url{https://www.escrypt.com/en/news-events/autosar_security}}, can be used to enhance the protection between components.
We describe the components that support dynamic and stateful identity management, and context-aware and continuous usage control of localised in-car resources.

\begin{figure}[!htb]
	\centering
	\includegraphics[width=\linewidth]{figures/siuv_archit.png}
	\caption{\acs{siuv} architecture}
	\label{fig:iam_architecutre}
\end{figure}

\textbf{\acp{idp}}
are authorities that issue identity \acp{vc} about drivers, passengers, applications or in-car resources.
An example of an issuer is the department of motor vehicles that issues driving licenses, or an \ac{oem} that asserts claims about an \ac{ecu}.

\textbf{A subject}
may be a driver, a passenger, an application or a resource that needs to access other resources, and is represented by a digital wallet that holds the subject's \acp{vc}.
The digital wallet interacts with the issuers and the \ac{sts} to present/obtain \acp{vc}, and with \acp{ucsp} to request access to resources.

\textbf{The \ac{vdr}}
is a distributed hashtable that holds revocation lists and issuers' public keys.
The \ac{vdr} may also be used to store other relevant information like metadata about proofs or schemas and structures of \acp{vc}.

\textbf{The \ac{sts}} is the core component that provides dynamic identity and privilege management of subjects.
It exchanges external identity \acp{vc} with internal privilege \acp{vc}.
The combination of \acp{vc} and the \ac{sts} protects against identity theft as privileges issued by the \ac{sts} are cryptographically verifiable and cannot be manipulated.
This also allows \textit{unlinkability} and maximizes privacy of subjects because they do not need to share identity information with the \acp{oem} of the vehicle's components.
It runs on the centralized general-purpose gateway, which has enough computational power to support the functionalities of the \ac{sts} as discussed in Section \ref{sec:test_cases}.


\textbf{Localised \acp{ucsp}} are used to protect resources such as domain controllers or \acp{ecu}.
However, they do \emph{not} control the low level behaviour of such components as this imposes a safety risk.
Rather, they only control the usage of high level APIs, services and functions exposed by such components.
Localised \acp{ucsp} make usage decisions according to resource and localised context attributes as well as privileges issued by the \ac{sts}.
Thus, the \ac{sts} handles the global context of the whole vehicle, while localised \acp{ucsp} manage localised authorisation contexts of individual components.
To enforce the \ac{polp}, we modified the physical architecture of \ac{ucsp} by making the \ac{pep} act as the \ac{ar} of the \ac{pip}.
Therefore, the \ac{pip} invokes the \ac{pep} to collect the required privileges and the \ac{pep}, in turn, requests these privileges from the wallet.
Accordingly, the flow of enforcing the \ac{polp} is shown in Figure \ref{fig:polp} and described as follows:
\begin{enumerate*}[label=(\arabic*)]
	\item the subject sends a request to the \ac{pep};
	\item the \ac{pep} creates an access request and invokes the \ac{ch};
	\item the \ac{ch} determines the required attributes to evaluate the applicable polices and invokes the corresponding \acp{pip};
	\item if the required attribute is a privilege, the \ac{pip} asks the \ac{pep} for the attribute;
	\item the \ac{pep} asks the wallet for the required privileges and the wallet presents them if they exist;
	\item the \ac{pep} verifies that the privileges were issued by the \ac{sts}, parses them and sends the value back to the \ac{pip};
	\item finally, the \ac{pip} returns the values to the \ac{ch}, which invokes the \ac{pdp} to evaluate the policy and make a decision.
\end{enumerate*}

\begin{figure}[!htb]
    \centering
	\includegraphics[scale=0.45]{figures/polp.png}
	\caption{Sequence diagram of enforcing the \ac{polp}}
	\label{fig:polp}
\end{figure}

\subsection{Revocation of \acp{vc}}
When an \ac{idp} revokes an external \ac{vc}, such as a driving license, the \ac{idp} updates the revocation list in the \ac{vdr}.
The update then gets communicated to all participating entities through the \ac{vdr}.
If the \ac{sts} is using the updated \ac{vc} in an active session, then a policy re-evaluation will be triggered in that session. 
Based on the policy re-evaluation, the \ac{sts} may revoke all internal \acp{vc} that were issued when the revoked external \ac{vc} was still valid.
When the \ac{sts} revokes internal \acp{vc}, it also updates the \ac{vdr}, thus the update gets communicated to all localised \acp{ucsp}.
This triggers a re-evaluation in the \ac{ucsp} sessions that are using the revoked internal \ac{vc}, and the corresponding access decisions will be updated according to the policy re-evaluation.
We assume that \acp{oem} define safety procedures in the policies they install on localised \acp{ucsp} so that revocation of access does not cause safety threats.
For instance, \acp{oem} may define policy obligations that safely stop the vehicle before completely revoking access or delay revocation of access until the vehicle stops.