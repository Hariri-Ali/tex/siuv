\section{Experimental Evaluation}\label{sec:evaluation}
In this section, we describe two use-cases and present the outcome of the experimental evaluation of \ac{siuv}.

\subsection{Use Case}\label{sec:usecase}
One possible use case of the proposed \ac{iam} system is travelling between the borders of two countries where driving rules are different.
For instance, the minimum age to drive is 17 in Denmark and 18 in Sweden.
Thus, we assume a 17-year-old driver with a valid driving license in Denmark.
The driver presents the license and ID credentials to the \ac{sts} in the car and the \ac{sts} issues a ``canDrive'' privilege.
This privilege is then used by localised \ac{ucsp} instances to grant access to the necessary components that allow the driver to drive the car.
As soon as the car crosses the borders to Sweden, the location attribute is updated and the \ac{sts} re-evaluates relevant policies.
In this case, the \ac{sts} finds that the ``canDrive'' privilege is not valid anymore, so it revokes it.
This is demonstrated in the non-inclusive policies of Listing \ref{lst:driver_sts_policy}.
The localised \ac{ucsp} instances receive the revocation update from the \ac{vdr}, and revoke access to the protected components in a safely manner.
However, policies of localised \ac{ucsp} instances may include obligations that can take safety-related actions upon the revocation of privileges as shown in Listing \ref{lst:driver_ucs_policy}.
This use case demonstrates that \acs{siuv} supports both centralised and distributed control.
Centralised control is performed by the \ac{sts} that monitors the global context and verifies credentials that are relevant to all resources.
Distributed control is enforced by localised \ac{ucsp} instances that monitor local contexts relevant to the protected resources only.
\acs{siuv} allows the car to monitor both global and local contexts and react accordingly.
Alternative solutions that only support centralised control cannot react to changes that are only relevant to a particular resource.
On the other hand, solutions that only support distributed control introduce a performance overhead because global context and attributes have to be monitored by all nodes.

A car rental service is also another relevant use-case of \acs{siuv} as car owners can restrict the use of their cars according to rental agreements.
For instance, a car owner can restrict the mobility of their rented car to a specific city or specific area. 
Thus, if the driver goes beyond the restricted area, \acs{siuv} can warn the driver or perhaps engage the autopilot to safely stop the car as defined by policies.
A car owner can also use \acs{siuv} to limit the maximum speed that can be reached by the driver who rents the car.
Moreover, a car owner can define a specific time period during which the car can be used by the driver according to the rental agreement.
Owners can also define policies that deny access to resources that are not needed by drivers who rent the car (e.g. diagnostic interfaces).



\begin{lstlisting}[float=!tbh,caption={STS policies},label={lst:driver_sts_policy}]
policySet privileges {
    apply permitUnlessDeny
    policy driver {
        target clause Attributes.isRegistered
        apply firstApplicable
        rule driver_dk {
            target clause Attributes.license.expiry > time.now()
                      and Attributes.license.issuer == ''borger.dk''
                      and Attributes.location == ''Denmark''
            condition Attributes.age > 17 and Attributes.ucs.step == ''ongoing''
            permit
            on permit {
                obligation canDrive { command = ''issue_privileges''
                                      canDrive = true }
        }}
        rule driver_dk_eu {
            target clause Attributes.license.expiry > time.now()
                      and Attributes.license.issuer == ''borger.dk''
            condition Attributes.age > 18 and Attributes.ucs.step == ''ongoing''
            permit
            on permit {
                obligation canDrive { command = ''issue_privileges''
                                      canDrive = true }
        }}
        rule revoke {
            deny
            on deny {
                obligation canDrive { command = ''revoke_privileges''
                                      canDrive = false }
    }}};
    policy wiperControl {  ...  };
    };
\end{lstlisting}

\begin{lstlisting}[float=!tbh, caption={STS policies},label={lst:driver_ucs_policy}]
policy engineControl {
    apply firstApplicable
    target clause Attributes.resourceId == ''engine''
    rule drive {
        target clause Attributes.api == ''engineAPI'' and Attributes.action == ''startEngine''
        condition Attributes.canDrive == true and Attributes.ucs.step == ''ongoing''
        permit
    }
    rule revoke {
        condition Attributes.ucs.step == ''post''
        deny
        on deny {
            obligation safeStop { command = ''autopilot''
                                  action = ''safe_stop'' }
    }}};
\end{lstlisting}

\subsection{Test Cases}\label{sec:test_cases}
We implemented \ac{ucsp} and \acs{siuv} using C++ and evaluated them on a computer running Ubuntu 20.04LTS Linux OS with Core i7-9850H CPU.
Modern car-ECU specifications, such as \cite{exynos}, include high-end processors and higher memory, which closely compares to the computational resources we use in our simulation, since \ac{ucsp} is highly optimised for similar systems as mentioned in \ref{sec:ucsp}.
We measured the time required to evaluate \ac{sts} and \ac{ucsp} policies as well as the overhead cost of an increasing number of attributes.
We also measured the time required to issue and verify \acp{vc} as well as the effect of the number of claims on the issuance and verification time. 
Finally, we evaluated the performance penalty of enforcing the \ac{polp} especially with an increasing number of privileges.
The results are shown in Figure \ref{fig:line_graphs} and discussed in the following subsections.

\textbf{Performance of Policy Evaluation in \ac{ucsp}:}
For this evaluation, we ran five tests with an increasing number of attributes that need to be collected by \acp{pip}.
It is necessary to note that this usually depends on the \acp{pip} and how they collect attribute values.
For instance, if a \ac{pip} needs to retrieve an attribute value over the network, then the network delay would affect the policy evaluation time.
However, \ac{ucsp} caches attributes in the \ac{at}, so only policy evaluations that need an uncached attribute value would be affected by such overhead. 
For this reason, we only measure the time required for policy evaluation when the attributes are already cached.
We ran each test 1000 rounds and observed a standard deviation of 19.2µs.
Table \ref{table:eval_perf} and Figure \ref{fig:line_graphs} show the average time required for policy evaluation in each test.
The results demonstrate that our implementation is lightweight, very efficient and highly optimised, which make it suitable for embedded and safety-critical systems like vehicles.

\textbf{Performance Evaluation of Issuing and Verifying \acp{vc}:}
We ran five different test cases with an increasing number claims.
In each case, we ran the test 1000 rounds and computed the average time and the standard deviation.
The observed standard deviation was 13.8µs and the results are shown in Table \ref{table:vc_perf}.
The results show that the number of claims does not have a significant effect on the efficiency of issuing and verifying \acp{vc}.
In addition, they show that the time required to issue or verify a typical \ac{vc} is less than 0.5ms.
This is because we used Ed25519 digital signatures, which are faster than any ECDSA or EdDSA instances in terms of signature generation and verification.

%\subsubsection{Performance Evaluation of the \ac{polp} Enforcement}
\textbf{Performance Evaluation of the \ac{polp} Enforcement:}
The implementation of the \ac{polp} includes back and forth communication between the wallet and the \ac{pep}, and between the \ac{pep} and the \ac{pip}.
Undoubtedly, this introduces a performance overhead especially because the \ac{pip} asks for privileges sequentially and synchronously.
To measure this overhead, we conducted 4 test cases with an increasing number of privileges.
The average time to enforce the \ac{polp} was computed as well as the standard deviation after running each test case 1000 times.
We noted a standard deviation of 44.7µs.
The results presented in Table \ref{table:polp_perf} show that the overhead increases significantly and linearly as the number of required privileges increases.
However, the number of privileges required for a specific action is not expected to be high, so such overhead can be tolerated.
Nonetheless, we plan to explore more efficient approaches to enforce the \ac{polp}.

\begin{table}[!htb]
	\centering
	\caption{Average time to evaluate \ac{alfa} policies as the\\ number of attributes increases}
	\begin{tabular}{ | c || c | c | c | c | c | }
		\hline
		\textbf{Number of attributes} & \textbf{1} & \textbf{3} & \textbf{5} & \textbf{10} & \textbf{20} \\ 
		\hline
		Average time to evaluate a policy (in µs) & 9 & 11 & 18 & 27 & 29 \\  
		\hline
	\end{tabular}
	\label{table:eval_perf}
\end{table}

 \begin{table}[!htb]
 \parbox{0.51\linewidth}{
     \centering
     \caption{Average time to issue and verify a \ac{vc} as function of the number of claims}
     \label{table:vc_perf}
     \resizebox{0.49\textwidth}{!}{
	\begin{tabular}{ | c || c | c | c | c | c | }
		\hline
		\makecell{\textbf{No. of claims $\rightarrow$}\\ \textbf{/ Avg. time (µs) $\downarrow$}} & \textbf{1} & \textbf{3} & \textbf{5} & \textbf{10} & \textbf{20} \\ 
		\hline
	    Issue a \ac{vc} (µs) & 86 & 97 & 105 & 136 & 196 \\  
		\hline
		Verify a \ac{vc} (µs) & 115 & 119 & 125 & 142 & 172 \\
		\hline
	\end{tabular}
     }
 }
 \hfill
 \parbox{0.49\linewidth}{
     \centering
     \caption{Average time to enforce the \ac{polp} as function of privilege number}
     \label{table:polp_perf}
     \resizebox{0.49\textwidth}{!}{
	\begin{tabular}{ | c || c | c | c | c | }
		\hline
		\makecell{\textbf{No. of privileges $\rightarrow$}\\ \textbf{/ Avg. time (µs) $\downarrow$}} & \textbf{1} & \textbf{3} & \textbf{5} & \textbf{10} \\ 
		\hline
		Enforce the \ac{polp} & 300 & 952 & 1554 & 3136 \\
		\hline
	\end{tabular}
     }
 }
 \end{table}


\begin{figure}[!htb]
	\centering
	\pgfplotsset{width=\linewidth}

	\begin{minipage}[t]{.32\textwidth}
		\begin{tikzpicture}
			\begin{axis}[
				axis lines=middle,
				ymin=5,
				ymax=40,
				x label style={at={(current axis)},anchor=south, below=20mm},
				title={\small{\ac{alfa} policy evaluation}},
				xlabel=No. of attributes,
				ylabel=Time in µs,
				xticklabel style={anchor=north},
				enlargelimits=false,
				xticklabels from table={eval_perf.dat}{attr},xtick=data]
				\addplot[red,thick,mark=square*] table [y=eval,x=X]{eval_perf.dat};
			\end{axis}
		\end{tikzpicture}
	\end{minipage}\hfill
	\begin{minipage}[t]{.32\textwidth}
		\begin{tikzpicture}
			\begin{axis}[
				axis lines=middle,
				ymin=50,
				ymax=250,
				x label style={at={(current axis)},anchor=south, below=20mm},
				title={\small{\ac{vc} issuance and verification}},
				xlabel=No. of claims,
				ylabel=Time in µs,
				xticklabel style={anchor=north},
				legend columns=2, 
				legend style={at={(0.5,1)},anchor=north, above=7mm},
				enlargelimits=false,
				xticklabels from table={vc_perf.dat}{claims},xtick=data]
				\addplot[orange,thick,mark=square*] table [y=issuance,x=X]{vc_perf.dat};
				\addlegendentry{Issuance}
				\addplot[green,thick,mark=square*] table [y= verification,x=X]{vc_perf.dat};
				\addlegendentry{Verification}]
			\end{axis}
		\end{tikzpicture}
	\end{minipage}\hfill
	\begin{minipage}[t]{.32\textwidth}
		\begin{tikzpicture}
			\begin{axis}[
				axis lines=middle,
				ymin=200,
				ymax=3500,
				x label style={at={(current axis)},anchor=south, below=20mm},
				title={\small{\ac{polp} overhead}},
				xlabel=No. of privileges,
				ylabel=Time in µs,
				xticklabel style={anchor=north},
				enlargelimits=false,
				xticklabels from table={polp_perf.dat}{privileges},xtick=data]
				\addplot[blue,thick,mark=square*] table [y=polp,x=X]{polp_perf.dat};
			\end{axis}
		\end{tikzpicture}
	\end{minipage}
	\caption{Performance results of issuing and verifying \acp{vc}, and enforcing the \ac{polp}}
	\label{fig:line_graphs}
\end{figure}