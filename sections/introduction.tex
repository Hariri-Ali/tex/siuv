\section{Introduction}\label{sec:introduction}
The exponential growth of the \ac{iov} innovations, such as autonomous driving, driver assistance, vehicle connectivity, infotainment and shared mobility has recently gained a notable attention in the automotive industry.
Such services and innovations are supported by the integration of smart sensors as well as the shift from electromechanical to interconnected software-based systems in modern vehicles \cite{mck2018architecture}\cite{mck2019race}.
In addition, vehicular systems are transforming towards centralised architectures in order to support dynamic services and functionalities.
Particularly, \acp{soa} are increasingly being adopted in automotive frameworks, due to their flexibility. 
\acp{soa} provide a better abstraction and separation between hardware and software, and allow smoother changes and integration of services.
However, the flexibility and dynamicity of \acp{soa} comes at the expense of the simple security of the statically predefined functions of existing architectures.
Although \acp{soa} are being adopted in automotive architectures, typical \ac{soa} security measures are not completely sufficient due to the safety-critical nature of vehicles \cite{rumez2020overview}.

The security of modern vehicles has gained more focus in the research community and the automotive industry.
For instance, Miller and Charlie \cite{miller2015remote} performed an attack that allowed them to gain access to critical physical systems such as steering and braking systems.
As a result, few millions of cars had to be recalled from the market.
Similarly, Dürrwang et al. \cite{durrwang2017security} exploited a vulnerability that allows an unintended deployment or a prevention of deployment of airbags.
More recently, Wouters et al. \cite{wouters2019fast} uncovered a vulnerability that allowed them to clone a key fob of a Tesla Model S in less than two minutes.
Researchers have also studied the state of the art of automotive security and defined open problems and challenges \cite{rumez2020overview}\cite{bernardini2017security}.
Dynamic access control and identity management are among the critical challenges that need to be addressed, because vehicles are transforming into digital systems where applications are provided by third party providers.
The behaviour of such applications is dynamic and cannot be known in advance, so they cannot be trusted unconditionally.
Therefore, such applications must be continuously verified and their internal and external communications and access to resources must be controlled and monitored \cite{rumez2020overview}\cite{bernardini2017security}\cite{mck2020challenge}.

Several researchers have proposed vehicular access control frameworks to address the aforementioned challenges.
For instance, Hamad et al. \cite{hamad2017secure} introduced an authentication and authorisation framework, based on a trust management model, that blocks or allows communications initiated by in-car applications
Kim et al. \cite{kim2016introducing} developed a decentralised access control framework by integrating an \ac{abac} module in AUTOSAR adaptive platform \cite{autosar2018adaptive}.
Rumez et al. \cite{rumez2019integration} also introduced a distributed \ac{abac} tailored for automotive architectures. 
Both \cite{kim2016introducing} and \cite{rumez2019integration} focused on protecting \ac{ecu} diagnostic interfaces from unauthorised access.
Likewise, Ammar et al. \cite{ammar2020securing} developed an end-to-end \ac{rbac} mechanism that regulates access to \ac{obd2} ports.

In spite of the significant contributions of the aforementioned works, context awareness, continuity of control and dynamic identity management remain overlooked.
These aspects are crucial because vehicles are real-time mobile systems whose environmental conditions change continuously as they move. 
Thus, access to vehicular resources must be continuously monitored and controlled to ensure correct, safe and secure usage as circumstances change.
The \ac{polp} is also another challenge that need to be addressed in order to mitigate insider threats and eliminate the risk of unintended or malicious use of unnecessary capabilities.
Furthermore, identities and privileges of subjects (i.e. drivers, passengers and applications) need to be continuously managed, monitored and updated according to contextual changes. 
To address these challenges, we present \ac{ucsp}: an optimised, efficient and modular implementation of the \ac{ucon} model tailored for embedded and safety-critical systems such as smart cars.
More importantly, we introduce \acs{siuv}: a vehicular \ac{iam} system that supports dynamic and stateful identity management, context-aware and continuous usage control, as well as the \ac{polp}.
\acs{siuv} incorporates a centralised stateful and dynamic \ac{sts} that manages, authenticates and verifies identities, and issues privileges in exchange using \acp{vc}.
The \ac{sts} exchanges external \acp{vc} that hold identity claims about subjects with internal \acp{vc} that enclose subject privileges and capabilities.
The \ac{sts} uses policy-based decision making to that defines how to exchange identity claims with privileges taking environmental conditions into account.
It also uses continuous monitoring and policy re-evaluation in order to manage the life-cycle of privileges and adapt to changing situations.
Following the trend of adopting \acp{soa} in vehicles, we also propose an \ac{soa}-based vehicular \ac{iam} architecture that consists of the aforementioned \ac{sts} in addition to distributed \acp{ucsp} that protect localised in-car resources.
We only focus on protecting internal vehicular resources, but \acs{siuv} can be extended to control external communications.
To sum up, the main contributions of this work are as follows:


\begin{itemize}
    \item \textbf{Dynamic and continuous authorisation:} optimised, efficient and modular implementation of the \ac{ucon} model (Section \ref{sec:ucsp})
    \item \textbf{Context-aware \ac{sts}:} policy-based \ac{sts} that manages a continuous life-cycle of privileges and adapts to changing situations (Section \ref{sec:sts})
    \item \textbf{\acs{siuv}:} \ac{soa}-based dynamic and context-aware \ac{iam} system (Section \ref{sec:siuv_architecture})
\end{itemize}

The rest of this paper is organised as follows:
Section \ref{sec:background} presents the technical background, namely the \ac{ucon} model, the used policy language, and \acp{vc}.
We describe \ac{ucsp} as well as \acs{siuv} architecture and \ac{sts} in Section \ref{sec:siuv}.
An example use case and the evaluation of the system are discussed in Section \ref{sec:evaluation}.
Finally, we define future directions and draw conclusions in Section \ref{sec:conclusion}.