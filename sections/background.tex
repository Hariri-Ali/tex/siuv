\section{Background}\label{sec:background}
This section outlines the theoretical and technical background of \acs{siuv}.

\subsection{Access and Usage Control}
\ac{abac} \cite{abac} is one of several access control models used to protect digital resources.
It provides more flexibility and finer-grained control than preceding models as it manages access rights based on attributes of subjects, resources and the environment.
\ac{abac}'s evaluation semantics, architecture, administration and policy language are defined in a comprehensive standardised reference model known as the \ac{xacml} \cite{xacml}.
Although \ac{abac} is fine-grained and flexible, it does not support continuity of access and mutability of attributes during evaluation.
For this reason,the \ac{ucon} model was proposed by Park and Sandhu \cite{ucon} as a generalisation that goes beyond \ac{abac} and other models to support context awareness, mutability of attributes and continuity of control.
\ac{ucon} continuously monitors attribute values and re-evaluates policies when a change occurs in order to guarantee that access rights still hold whilst usage is still in progress.
The model categorises decision predicates as ``\emph{pre}'', ``\emph{ongoing}'' and ``\emph{post}''.
``\emph{pre}'' predicates are evaluated when an access request is made in order to decide whether to grant or deny access;
``\emph{ongoing}'' predicates are evaluated during the time span of access, and they decide whether to revoke or retain access;
and ``\emph{post}'' predicates are evaluated after the end/revocation of access.
In addition, \ac{ucon} introduces obligations and advice; such that obligations are actions that must be fulfilled, whereas advice refers to actions that are recommended.
The novelties of \ac{ucon} make it an excellent baseline for dynamic applications, such as \ac{iov}, where the context changes continuously and resource usage is long-lived.

\subsection{\acf{alfa}}
\ac{alfa} \cite{alfa-oasis} is a pseudocode domain-specific policy language that maps directly to \ac{xacml} without adding any new semantics.
It is much less verbose than \ac{xacml} and thus more human readable and shorter in size.
We use \ac{alfa} as the baseline policy language of the \ac{iam} because its compact size allows faster parsing and evaluation, which is imperative in safety-critical applications like smart vehicles.
In addition, a policy-based \ac{iam} allows a dynamic and codeless behaviour that adapts according to the context.

\subsection{\acfp{vc}}
Verifiable Credentials are digital identifiers that provide cryptographic proofs to support the validity and reliability of a claim.
The W3C \ac{vc} data model \cite{vc2019datamodel} specifies the roles of claims, credentials, presentations etc. to help conform to a common structure.
We construct such claims as privileges, which authorise a subject to use specified car resources.
If we consider a \ac{vc} as a presentation graph, it could consist of different credentials which are linked to each other contextually and packaged together to give a unique presentation by the holder.
Access to car resources requires appropriate authorisation and should be controlled through credentials and continuously monitored through policies and usage-checks.
A \ac{vc} is suited to store such credentials because it is cryptographically verifiable and tamper-resistant.
The set of claims that a \ac{vc} makes on behalf of the issuer or multiple issuers can refer to a common or multiple subjects too, thus making the case for multiple identity properties be used for a distinct purpose.
The \ac{vc} information flow allows an issuer to issue credentials to the holder.
The credentials are stored in a wallet and the holder decides whether to present the information in the form of a credential or presentation to the verifier.
Every credential or presentation includes a proof mechanism that helps the verifier to verify the authenticity of the \ac{vc} with the \ac{vdr}.
We adhere to the W3C \ac{vc} model \cite{vc2019datamodel} that specifies that every \ac{vc} must include a proof mechanism to support the verifiability of the credentials.
We used Ed25519 \cite{ed25519}, a twisted Edwards curve digital signature algorithm, based on elliptic curve cryptography.
As of today, Ed25519 is the most popular instance of EdDSA and is based on the Edwards Curve25519.
Although there are many variants of Ed25519-original \cite{ed25519} such as NIST \cite{chen2019recommendations}, IETF \cite{josefsson2017rfc8032} etc. that specifies some refined security properties, we use the Ed25519 instance by LibSodium \cite{denis2017libsodium}, a widely popular cryptographic library.
Brendel et al. \cite{brendel2020provable} discusses the game-based definitions of the security properties of the Ed25519 signature scheme and provably defines Ed25519-LibSodium \cite{denis2017libsodium} to be more resilient against key substitution attacks as well as message bound security than other Ed25519 instances \cite{ed25519}\cite{chen2019recommendations}\cite{josefsson2017rfc8032}.